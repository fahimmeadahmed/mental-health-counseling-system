var express     = require('express');
var userListModel = require('../models/userList');
var router      = express.Router();


router.get('/', function(req, res){

    userListModel.getAll(function(results){
        var data={results:results}
        res.render('staff/userList',data);
    });
});
router.get('/delete/:id', function(req, res){
        userListModel.get(req.params.id,function (result){

    res.render('staff/delete', {user:result});

    });
});
router.post('/delete/:id', function(req, res){

    userListModel.delete(req.body.id, function(status){
        if(status){
            res.redirect('/userList');
        }else{
            res.redirect('/userList');
        }
    });
});
module.exports = router;